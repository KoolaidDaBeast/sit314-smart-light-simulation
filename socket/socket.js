var app = require('express')();
const cors = require('cors');

app.use(cors());

const server = require('http').Server(app);
const io = require('socket.io')(server);

const port = process.env.PORT || 3000;
var light_data = undefined;
const MAX_ROOMS = 50;
const MAX_LIGHTS = 10;
const MAX_NODES = 10;

console.log("Server will be listening on port " + port);
server.listen(port);

function emit(io, namespace, room, data) {
    io.of('/' + namespace).emit(room, JSON.stringify(data));
}

function generateLightArray(status) {
    var array = [];
    for (var i = 0; i < MAX_LIGHTS; i++) {
        array.push(`${i}=${status}`);        
    }

    return array;
}

function generateRoomsStaus() {
    var result = {};

    for (var room = 0; room < MAX_ROOMS; room++) {
        var item = light_data[`B${room + 1}`];  

        result[room] = "on";
        
        if (JSON.stringify(item).includes("off")) {
            result[room] = "off";
            continue;
        }
    }

    return result;
}

function generateRoomStaus(room) {
    var result = {};

    for (var i = 0; i < MAX_NODES; i++) {
        var item = light_data[room][i];  

        result[i] = "on";
        
        if (JSON.stringify(item).includes("off")) {
            result[i] = "off";
            continue;
        }
    }

    return result;
}

app.get('/', function (req, res) {
    return res.send(light_data);
});

app.get('/:nodeid', function (req, res) {
    const { nodeid } = req.params;
    return res.send(light_data[nodeid]);
});

io.on("connection", function(socket) {

    if (light_data == undefined) {
        light_data = {};

        for (var room = 0; room < MAX_ROOMS; room++) {
            light_data[`B${room + 1}`] = {};

            for (var node = 0; node < MAX_NODES; node++) {
                light_data[`B${room + 1}`][node] = {};
                light_data[`B${room + 1}`][node] = generateLightArray("off");
            }
        }
    }    

    //Client Ping Server
    socket.on("connection_status", function(data) {
        socket.emit("connection_status", { message: 'pong'});
    });

    //Client Ping Server
    socket.on("light_event", function(data) {
        io.emit("light_event", data);

        //All lights
        if (data.node_id == "*" && data.type == "ACTION" && data.light_action.startsWith("*")) {
            for (var node = 0; node < MAX_NODES; node++) {
                light_data[data.head_id][node] = generateLightArray(data.light_action.split('=')[1]);
            }
        }

        //All lights in row
        if (data.node_id != "*" && data.type == "ACTION" && data.light_action.startsWith("*")) {
            light_data[data.head_id][data.node_id] = generateLightArray(data.light_action.split('=')[1]);
        }
    });

    //Send light data
    socket.on("setup_light_event", function(data) {

        if (data.head_id != "*" && data.node_id == "*") {
            socket.emit("setup_light_event", generateRoomStaus(data.head_id));
        }

        if (data.head_id == "*") {
            socket.emit("setup_light_event", generateRoomsStaus());
        }
    });
});